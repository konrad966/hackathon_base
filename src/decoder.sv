module decoder(
	input clk,

	input   [7 : 0]   i_rdata,
   input           vdata,
	
	output vout
);

reg [15 : 0] dst;
reg [15 : 0] src;
reg [15 : 0] size;
reg [1 : 0] d;
reg [6 : 0] Dtype;
reg [7 : 0] payload [0 : 41];

reg [7 : 0] cnt;
reg [7 : 0] cnt_pay;

reg   [5:0] state       = 0;
localparam  IDLE        = 0;
localparam  READ        = 1;
localparam  READ_PAYLOAD = 2;

always @(posedge clk)
begin
    case (state)
      IDLE: begin
			cnt <= 0;
			cnt_pay <= 0;
			vout <= 0;
			if (vdata) begin
				dst[15 : 8] <= i_rdata;
				state <= READ;
			end
		end
		READ: begin
			cnt <= cnt + 1;
			if (cnt == 1)
				dst[7 : 0] <= i_rdata;
			if (cnt == 2)
				src[15 : 8] <= i_rdata;
			if (cnt == 3)
				src[7 : 0] <= i_rdata;
			if (cnt == 4)
				size[15 : 8] <= i_rdata;
			if (cnt == 5)
				size[7 : 0] <= i_rdata;
			if (cnt == 6)
				d <= i_rdata[7];
				Dtype[6 : 0] <= i_rdata;
				state <= READ_PAYLOAD;
		end
		READ_PAYLOAD: begin
			if (cnt_pay < size) begin
				payload[cnt_pay] <= i_rdata;
				cnt_pay <= cnt_pay + 1;
			end
			else if (cnt < 42)
				cnt_pay <= cnt_pay + 1;
			else
				vout <= 1;
				state <= IDLE;		
		end
	endcase
end

endmodule