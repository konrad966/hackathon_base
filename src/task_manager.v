module task_manager(
  //  sys
  input           i_clk,      i_rst,
  output          o_link,
  //  eth
  input   [7:0]   i_rdata,
  input           i_rready,
  output          o_rreq,
  output  [7:0]   o_wdata,
  output [7:0] ostate,
  input           i_wready,
  output          o_wvalid
);

//  fsm - add task control here
reg [7:0]     state     = 0;
localparam    IDLE      = 0;
localparam    CONNECTED = 1;
localparam    LINKED    = 2;
localparam    ADDRESS_REQUEST = 3;
localparam    ADDRESS_REPLY = 4;

wire          done;
reg           link      = 0, send     = 0;
reg send_addr_req = 0;
wire done_addr_req;
wire done_addr_reply;
wire doing_addr_req;
reg access_granted = 0;



always @(posedge i_clk) begin
  if (i_rst) begin
    link  <= 0;
    send  <= 0;
    state <= IDLE;
  end
  else begin
    case (state)
      IDLE: begin
        link <= 0;
        if  (done) begin
          send  <=  1;
          state <= CONNECTED;
        end
      end
      CONNECTED: begin
        if (done) begin
          state <= LINKED;
          access_granted <= 0;
			  end
        else
          send  <=  0;
      end
      LINKED: begin
        if (done) begin
          link  <= 0;
          send  <= 1;
          state <= CONNECTED;
        end else begin
          if (!access_granted) begin
            state <= ADDRESS_REQUEST;
            send_addr_req <= 1;
            link <= 1; // change to 1 to enable MHP protocol ethertype usage
          end else begin
            link <= 0; // change to 1 to enable MHP protocol ethertype usage
          end
        end
      end
      ADDRESS_REQUEST: begin
        if (doing_addr_req) begin
          link <= 1;
        end
        if (done_addr_req) begin
          send_addr_req <= 0;
          state <= ADDRESS_REPLY;
        end
      end
      ADDRESS_REPLY: begin
        if (done_addr_reply) begin
          link <= 0;
          state <= LINKED;
          access_granted <= 1;
        end
      end
    endcase
  end
end

mhp protocol(
  //  sys
  .i_clk    (i_clk),
  .i_rst    (i_rst),
  //  ctrl
  .i_send   (send),
  .o_done   (done),
  //  eth
  .i_rdata  (i_rdata),
  .i_rready (i_rready),
  .o_rreq   (o_rreq),
  .o_wdata  (o_wdata),
  .i_wready (i_wready),
  .o_wvalid (o_wvalid),
  // task
  .send_addr_req(send_addr_req),
  .done_addr_req(done_addr_req),
  .doing_addr_req(doing_addr_req),
  .done_addr_reply(done_addr_reply),
  .ostate(ostate)
);

assign  o_link  = link;

endmodule