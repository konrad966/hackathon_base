`timescale 1ns/1ns

module mhp(
  //  sys
  input           i_clk,      i_rst,
  //  ctrl
  input           i_send,
  input send_addr_req,
  output doing_addr_req,
  output done_addr_req,
  output done_addr_reply,
  output doing_addr_reply,
  output          o_done,
  output [7:0] ostate,
  //  eth
  input   [7:0]   i_rdata,
  input           i_rready,
  output          o_rreq,
  output  [7:0]   o_wdata,
  input           i_wready,
  output          o_wvalid 
);

//  fsm
reg   [7:0] state       = 0;
localparam  IDLE        = 0;
localparam  READ        = 1;
localparam  WRITE       = 2;
localparam  WRITE_ADDR_REQ = 3;
localparam  READ_ADDR_REPLY = 4;

reg [7 : 0] GET_ADDR_DATA [0 : 8] = '{8'hFF, 8'hFF, 8'h0, 8'h0, 8'h0, 8'h0, 8'h83, 8'h00, 8'h00};
// reg [7 : 0] GET_ADDR_DATA [0 : 8] = '{8'hFF, 8'hFF, 8'h0, 8'h0, 8'h0, 8'h0, 8'h83, 8'h00, 8'h00};
//  local regs
reg           done      = 0;
reg _done_addr_req = 0;
reg _done_addr_reply = 0;
reg _doing_addr_req = 0;
reg _doing_addr_reply = 0;
//  read regs
reg           r_req     = 0;
//  write regs
reg   [7:0]   w_data    = 0;
reg           w_valid   = 0;
reg idata_valid = 0;

reg [9 : 0] bytes_sent = 0;

reg [31: 0] timeout_cnt = 0;
reg [31: 0] timeout_1s =  50000000;
reg [31: 0] timeout_05ms =  25000;
reg [31: 0] timeout_1ms =  50000;
reg [31: 0] timeout_2s = 100000000;
reg [31: 0] timeout_3s = 150000000;
reg [31: 0] timeout_4s = 200000000;
// reg [31: 0] timeout_1s = 50000;

always @(posedge i_clk) begin
  if (i_rst) begin
    done    <= 0;
    w_data  <= 0;
    w_valid <= 0;
    state   <= IDLE;
  end
  else begin
    case (state)
      IDLE: begin
        _done_addr_req <= 0;
        _done_addr_reply <= 0;
        w_data  <= 0;
        w_valid <= 0;
        done    <= 0;
        if (i_rready) begin // received frame's payload ready
          r_req   <= 1;     // r_req set before read state, so we can expect valid data in READ state
          state   <= READ;
        end else if (send_addr_req) begin
          state <= WRITE_ADDR_REQ;
        end else
          r_req   <= 0;
      end
      READ: begin
        if (i_rready) // clear fifo
          r_req   <= 1;
        else begin
          r_req   <= 0;
          done    <= 1;
          state   <= WRITE;
        end
      end
      WRITE: begin    //  write data
        if (i_wready) begin
          w_valid <= 1;
          state   <=  IDLE;
        end
      end
      WRITE_ADDR_REQ: begin    //  write data
        if (i_wready) begin
          _doing_addr_req <= 1;
        end
        if (_doing_addr_req) begin
			    if(bytes_sent == 9) begin
            w_valid <= 0;
            w_data  <= 0;
					  bytes_sent <= 0;
            _doing_addr_req <= 0;
					  _done_addr_req <= 1;
					  state   <=  READ_ADDR_REPLY;
          end else begin
            w_valid <= 1;
			      w_data <= GET_ADDR_DATA[bytes_sent];
			      bytes_sent <= bytes_sent + 1;
          end
        end
      end
      READ_ADDR_REPLY: begin    //  write data
        if (i_rready) begin // clear fifo
          r_req   <= 1;
          if (_doing_addr_reply) begin
            idata_valid <= 1;
          end
          _doing_addr_reply <= 1;
        end else begin
          if (_doing_addr_reply) begin
            idata_valid <= 0;
            timeout_cnt <= 0;
            _doing_addr_reply <= 0;
            _done_addr_reply <= 1;
            r_req <= 0;
            state <= IDLE;
          end
        end

        if (timeout_cnt == timeout_1ms) begin
            timeout_cnt <= 0;
            _doing_addr_reply <= 0;
            _done_addr_reply <= 1;
            r_req <= 0;
            state <= IDLE;
        end else begin
          timeout_cnt <= timeout_cnt + 1;
        end
      end
    endcase
  end
end


//GET ADDR MHP PACKET
localparam [15 : 0] GETADDR_DST = 16'hFFFF;
localparam [15 : 0] GETADDR_SRC  = 16'h0000;
localparam [15 : 0] GETADDR_SIZE = 16'h0000;
localparam GETADDR_D = 1'b1;
localparam [6 : 0] GETADDR_TYPE = 7'h03;
localparam [15 : 0] GETADDR_SCS = 16'h0509;

assign    o_done   = done;
assign    o_rreq   = r_req;
assign    o_wdata  = w_data;
assign    o_wvalid = w_valid;
assign    done_addr_req = _done_addr_req;
assign    done_addr_reply = _done_addr_reply;
assign    doing_addr_req = _doing_addr_req;
assign ostate = state;

endmodule
