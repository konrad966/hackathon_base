module compute_SCS(
	input clk,

	input [7 : 0] in_byte,
	input in_valid,
	
	output reg [15 : 0] sum,
	output out_valid
);


reg [3 : 0] mult_reg = 1;


always @(posedge clk)
begin
	if(in_valid)
	begin
		sum <= in_byte * mult_reg;
		mult_reg <= mult_reg << 1;
		
		if(mult_reg == 8)
			mult_reg <= 1;
	end
end

endmodule